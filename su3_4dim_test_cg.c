#include <stdio.h>
#include <stdlib.h>

#define N 6
#define V (N*N*N*N)
#define LEFT 0
#define RIGHT 1

double fabs(double x){
	if( x > 0)
		return x;
	else
		return -x;
}

class complex {
public:
	double r,i;
	complex(double rr,double ii);
	complex();
	complex& operator=(const complex &a);
	complex& operator=(const double &a);
};

complex::complex(double rr,double ii){
this->r = rr;
this->i = ii;
}

complex::complex(){
this->r = 0;
this->i = 0;
}

complex operator*(complex a, const complex &b){
complex c;
c.r = a.r*b.r - a.i*b.i;
c.i = a.r*b.i + a.i*b.r;
return c;
}

complex operator*(double a, const complex &b){
complex c;
c.r = a*b.r;
c.i = a*b.i;
return c;
}

complex operator+(complex a, const complex &b){
complex c;
c.r = a.r + b.r;
c.i = a.i + b.i;
return c;
}

complex operator-(complex a, const complex &b){
complex c;
c.r = a.r - b.r;
c.i = a.i - b.i;
return c;
}

complex& complex::operator=(const complex &a){

    if (this != &a) { 
	this->r = a.r;
	this->i = a.i;
    }

return *this;
}

complex& complex::operator=(const double &a){

    this->r = a;
    this->i = 0.0;

return *this;
}


class su3_matrix {
public:
	complex m[3][3];
	su3_matrix();
	void setUnity();
	void setRandom();
};

class su3_vector {
public:
	complex v[3];
	su3_vector();
	void setZero();
	void setOne();
	su3_vector& operator=(const su3_vector &a);

};

complex conjg(complex a){

	complex b;
	b.r = a.r;
	b.i = -a.i;

return b;
}

su3_vector conjg(su3_vector a){

	su3_vector b;
	b.v[0] = conjg(a.v[0]);
	b.v[1] = conjg(a.v[1]);
	b.v[2] = conjg(a.v[2]);

return b;
}


class su3_spinor {
public:
	su3_vector s[4];
	void setZero();
	void setOne();
	void setRandom();
	int isZero();
	void multiply_by_gamma5();
	su3_spinor& operator=(const su3_spinor &a);
};


su3_vector dagger(su3_vector a){

	su3_vector b;

	b.v[0] = conjg(a.v[0]);
	b.v[1] = conjg(a.v[1]);
	b.v[2] = conjg(a.v[2]);

return b;
}

su3_spinor dagger(su3_spinor a){

	su3_spinor b;

	b.s[0] = conjg(a.s[0]);
	b.s[1] = conjg(a.s[1]);
	b.s[2] = conjg(a.s[2]);
	b.s[3] = conjg(a.s[3]);

return b;
}


su3_matrix dagger(su3_matrix a){

	su3_matrix b;
	
	b.m[0][0] = conjg(a.m[0][0]);
	b.m[0][1] = conjg(a.m[1][0]);
	b.m[0][2] = conjg(a.m[2][0]);
	b.m[1][0] = conjg(a.m[0][1]);
	b.m[1][1] = conjg(a.m[1][1]);
	b.m[1][2] = conjg(a.m[2][1]);
	b.m[2][0] = conjg(a.m[0][2]);
	b.m[2][1] = conjg(a.m[1][2]);
	b.m[2][2] = conjg(a.m[2][2]);

return b;
}

su3_matrix transpose(su3_matrix a){

	su3_matrix b;
	
	b.m[0][0] = a.m[0][0];
	b.m[0][1] = a.m[1][0];
	b.m[0][2] = a.m[2][0];
	b.m[1][0] = a.m[0][1];
	b.m[1][1] = a.m[1][1];
	b.m[1][2] = a.m[2][1];
	b.m[2][0] = a.m[0][2];
	b.m[2][1] = a.m[1][2];
	b.m[2][2] = a.m[2][2];

return b;
}

su3_vector::su3_vector(){

v[0] = 0;
v[1] = 0;
v[2] = 0;

}

su3_matrix::su3_matrix(){

m[0][0] = 0;
m[0][1] = 0;
m[0][2] = 0;
m[1][0] = 0;
m[1][1] = 0;
m[1][2] = 0;
m[2][0] = 0;
m[2][1] = 0;
m[2][2] = 0;

}

void su3_vector::setZero(){

	v[0] = 0;
	v[1] = 0;
	v[2] = 0;	

}
void su3_vector::setOne(){

	v[0] = 1;
	v[1] = 1;
	v[2] = 1;	

}

int su3_spinor::isZero(){

	int r = 1;

	int t,c;
	for(t = 0; t < 4; t++){
		for(c = 0; c < 3; c++){

			if( (fabs(s[t].v[c].r) > 10e-12) || (fabs(s[t].v[c].i) > 10e-12) )
				r = 0;
		}
	}

return r;
}



void su3_spinor::setZero(){

	s[0].setZero();
	s[1].setZero();
	s[2].setZero();	
	s[3].setZero();	


}
void su3_spinor::setOne(){

	s[0].setOne();
	s[1].setOne();
	s[2].setOne();	
	s[3].setOne();	

}
void su3_spinor::setRandom(){

        int i,j;

        for(i = 0; i < 4; i++){
                for(j = 0; j < 3; j++){

                         s[i].v[j].r = 1.0*rand()/RAND_MAX;
                         s[i].v[j].i = 1.0*rand()/RAND_MAX;
             }
        }
}
void su3_matrix::setUnity(){

	m[0][0] = 1.0;
	m[1][1] = 1.0;
	m[2][2] = 1.0;	

}

void su3_matrix::setRandom(){

	int i,j;

	for(i = 0; i < 3; i++){
		for(j = 0; j < 3; j++){

			m[i][j] = 1.0*rand()/RAND_MAX;
		}
	}
}
su3_vector operator*(su3_matrix a, const su3_vector &b){

su3_vector c;

c.v[0] = a.m[0][0]*b.v[0] + a.m[1][0]*b.v[1] + a.m[2][0]*b.v[2];
c.v[1] = a.m[0][1]*b.v[0] + a.m[1][1]*b.v[1] + a.m[2][1]*b.v[2];
c.v[2] = a.m[0][2]*b.v[0] + a.m[1][2]*b.v[1] + a.m[2][2]*b.v[2];

return c;
}
su3_vector operator*(double alpha, const su3_vector &b){

su3_vector c;

c.v[0] = alpha*b.v[0];
c.v[1] = alpha*b.v[1];
c.v[2] = alpha*b.v[2];

return c;
}

su3_vector operator*(complex alpha, const su3_vector &b){

su3_vector c;

c.v[0] = alpha*b.v[0];
c.v[1] = alpha*b.v[1];
c.v[2] = alpha*b.v[2];

return c;
}

su3_spinor operator*(double alpha, const su3_spinor &b){

su3_spinor c;

c.s[0] = alpha*b.s[0];
c.s[1] = alpha*b.s[1];
c.s[2] = alpha*b.s[2];
c.s[3] = alpha*b.s[3];

return c;
}

su3_spinor operator*(complex alpha, const su3_spinor &b){

su3_spinor c;

c.s[0] = alpha*b.s[0];
c.s[1] = alpha*b.s[1];
c.s[2] = alpha*b.s[2];
c.s[3] = alpha*b.s[3];

return c;
}

complex operator*(const su3_vector a, const su3_vector &b){

complex x;

x = a.v[0]*b.v[0] + a.v[1]*b.v[1] + a.v[2]*b.v[2];

return x;
}

void su3_spinor::multiply_by_gamma5(){

	s[2] = (-1.0)*s[2];
	s[3] = (-1.0)*s[3];

}

complex operator*(const su3_spinor a, const su3_spinor &b){

complex x;

x = a.s[0]*b.s[0] + a.s[1]*b.s[1] + a.s[2]*b.s[2] + a.s[3]*b.s[3];
//x = a.s[2]*b.s[0] + a.s[3]*b.s[1] + a.s[0]*b.s[2] + a.s[1]*b.s[3];

return x;
}

su3_vector operator+(su3_vector a, const su3_vector &b){

su3_vector c;

c.v[0] = a.v[0] + b.v[0];
c.v[1] = a.v[1] + b.v[1];
c.v[2] = a.v[2] + b.v[2];

return c;
}

su3_vector operator-(su3_vector a, const su3_vector &b){

su3_vector c;

c.v[0] = a.v[0] - b.v[0];
c.v[1] = a.v[1] - b.v[1];
c.v[2] = a.v[2] - b.v[2];

return c;
}

su3_spinor operator+(su3_spinor a, const su3_spinor &b){

su3_spinor c;

c.s[0] = a.s[0] + b.s[0];
c.s[1] = a.s[1] + b.s[1];
c.s[2] = a.s[2] + b.s[2];
c.s[3] = a.s[3] + b.s[3];

return c;
}

su3_spinor operator-(su3_spinor a, const su3_spinor &b){

su3_spinor c;

c.s[0] = a.s[0] - b.s[0];
c.s[1] = a.s[1] - b.s[1];
c.s[2] = a.s[2] - b.s[2];
c.s[3] = a.s[3] - b.s[3];

return c;
}

su3_vector& su3_vector::operator=(const su3_vector &a){

    if (this != &a) { 
	this->v[0] = a.v[0];
	this->v[1] = a.v[1];
	this->v[2] = a.v[2];
    }
    return *this;
}

su3_spinor& su3_spinor::operator=(const su3_spinor &a){

    if (this != &a) { 
	this->s[0] = a.s[0];
	this->s[1] = a.s[1];
 	this->s[2] = a.s[2];
	this->s[3] = a.s[3];
   }
    return *this;
}


int set_initial_x(su3_spinor* x, char c){

	int i;

        switch(c){

        case 'u':

        for(i = 0; i < V; i++){
                x[i].setOne();
        }

        break;

        case 'r':

        for(i = 0; i < V; i++){
                x[i].setRandom();
        }

        break;

	}

return 1;
}

int set_initial_b(su3_spinor* b){

	int i;

	for(i = 0; i < V; i++){

		b[i].setZero();

	}

	b[0].s[0].v[0] = 1;
//	b[12].s[0].v[0] = 1;
//	b[56].s[0].v[0] = 1;

return 1;
}

int set_initial_b(su3_spinor* b, int k, int s, int c){

	int i;

	for(i = 0; i < V; i++){

		b[i].setZero();

	}

	b[k].s[s].v[c] = 1;
//	b[k].setRandom();

return 1;
}


int set_initial_U(su3_matrix U[V][4], char c){

	int i;

	switch(c){

	case 'u':

	for(i = 0; i < V; i++){

		U[i][0].setUnity();
		U[i][1].setUnity();
		U[i][2].setUnity();
		U[i][3].setUnity();

	}

	break;

	case 'r':

	for(i = 0; i < V; i++){

		U[i][0].setRandom();
		U[i][1].setRandom();
		U[i][2].setRandom();
		U[i][3].setRandom();

	}

	break;
	}

return 1;
}

int neighbour(int pos, int dir, int axis){

//int x = pos%N;
//int y = (pos/N)%N;
//int z = (pos/N/N)%N;
//int t = (pos/N/N/N)%N;

int t = (int)(pos/(N*N*N));
int z = (int)(((pos)%(N*N*N))/(N*N));
int y = (int)(((pos)%(N*N*N))%(N*N)/N);
int x = (int)((((pos)%(N*N*N))%(N*N))%N);

int x_new, y_new, z_new, t_new;

//first simply take the neighbour by adding +/-1 (FWD=1,BWD=0 => +1=2*FWD-1, -1=2*BWD-1)

        switch(axis){
            case 0 :
                x_new = x + 2*dir-1;
		y_new = y;
		z_new = z;
		t_new = t;
		break;
	
            case 1:
                x_new = x;
		y_new = y + 2*dir-1;
		z_new = z;
		t_new = t;
		break;

            case 2 :
		x_new = x;
		y_new = y;
                z_new = z + 2*dir-1;
		t_new = t;
		break;
	
            case 3:
                x_new = x;
		y_new = y;
		z_new = z;
		t_new = t + 2*dir-1;
		break;

	}

        if(x_new < 0){
            x_new = N-1;
	}else if(x_new > N-1){
            x_new = 0;
	}

        if(y_new < 0){
            y_new = N-1;
	}else if(y_new > N-1){
            y_new = 0;
	}

        if(z_new < 0){
            z_new = N-1;
	}else if(z_new > N-1){
            z_new = 0;
	}

        if(t_new < 0){
            t_new = N-1;
	}else if(t_new > N-1){
            t_new = 0;
	}

return t_new*N*N*N + z_new*N*N + y_new*N + x_new;
}

su3_spinor r[V], b[V], tmp, p[V], x[V], rb[V], pb[V];
su3_spinor rprev[V], rbprev[V], pprev[V], pbprev[V];
su3_spinor tmp_dagger[V];
su3_spinor tmp_dagger2[V];
su3_matrix U[V][4];


su3_spinor multiply_by_A(su3_spinor *x, int i){

su3_spinor result;

su3_matrix link;

double scale_off_diagonal = -0.5;
int dir,ii;
complex j(0,-1.0);

//         direction, left/right, components
su3_vector u[4][2][4];
su3_vector w[4][2][4];

//LEFT
int ind;
int lr;
double signlr; //can be changed to signed int?

for(lr = 0; lr < 2; lr++){ 

	signlr = 2.0*lr-1.0;  //+- 1

	dir = 0;
	ind = neighbour(i,lr,dir);
	u[dir][lr][0] = x[ind].s[0] - (signlr*j)*(x[ind].s[3]);
	u[dir][lr][1] = x[ind].s[1] - (signlr*j)*(x[ind].s[2]);
	u[dir][lr][2] = (signlr*j)*(u[dir][lr][1]);
	u[dir][lr][3] = (signlr*j)*(u[dir][lr][0]);

	if(lr == 0){
		link = dagger(U[ind][dir]);
	}else{
		link = U[i][dir];
	}

	for(ii = 0; ii < 4; ii++){
		w[dir][lr][ii] = link*u[dir][lr][ii];
	}


	dir = 1;
	ind = neighbour(i,lr,dir);
	u[dir][lr][0] = x[ind].s[0] + signlr*x[ind].s[3];
	u[dir][lr][1] = x[ind].s[1] - signlr*x[ind].s[2];
	u[dir][lr][2] = (-signlr)*u[dir][lr][1];
	u[dir][lr][3] = signlr*u[dir][lr][0];
	
	if(lr == 0){
		link = dagger(U[ind][dir]);
	}else{
		link = U[i][dir];
	}

	for(ii = 0; ii < 4; ii++){
		w[dir][lr][ii] = link*u[dir][lr][ii];
	}


	dir = 2;
	ind = neighbour(i,lr,dir);
	u[dir][lr][0] = x[ind].s[0] - (signlr*j)*x[ind].s[2];
	u[dir][lr][1] = x[ind].s[1] + (signlr*j)*x[ind].s[3];
	u[dir][lr][2] = (signlr*j)*u[dir][lr][0];
	u[dir][lr][3] = (-signlr*j)*u[dir][lr][1];
	
	if(lr == 0){
		link = dagger(U[ind][dir]);
	}else{
		link = U[i][dir];
	}	
	
	for(ii = 0; ii < 4; ii++){
		w[dir][lr][ii] = link*u[dir][lr][ii];
	}


	dir = 3;
	ind = neighbour(i,lr,dir);
	u[dir][lr][0] = x[ind].s[0] - signlr*x[ind].s[2];
	u[dir][lr][1] = x[ind].s[1] - signlr*x[ind].s[3];
	u[dir][lr][2] = -signlr*u[dir][lr][0];
	u[dir][lr][3] = -signlr*u[dir][lr][1];
	
	if(lr == 0){
		link = dagger(U[ind][dir]);
	}else{
		link = U[i][dir];
	}

	for(ii = 0; ii < 4; ii++){
		w[dir][lr][ii] = link*u[dir][lr][ii];
	}

}

result.setZero();

//diagonal term
result = result + (4.001*x[i]);

//offdiagonal
for(lr = 0; lr < 2; lr++)
	for(dir = 0; dir < 4; dir++)
		for(ii = 0; ii < 4; ii++)
			result.s[ii] = result.s[ii] + scale_off_diagonal*w[dir][lr][ii];


return result;
}

su3_spinor multiply_by_Adagger(su3_spinor *x, int i){

su3_spinor tmp[V], result;

int ii;

for(ii = 0; ii < V; ii++){

	tmp[ii] = x[ii];
	tmp[ii].multiply_by_gamma5();
}
	
result = multiply_by_A(tmp, i);

result.multiply_by_gamma5();

return result;
}

/*
int print_dirac(void){

int i, s, c;
int ii, ss, cc, iii;

su3_spinor bb[V], bbb[V];
su3_spinor ttmp, ttmp2, ttmp3;

for(i = 0; i < V; i++){
	for(s = 0; s < 4; s++){
		for(c = 0; c < 3; c++){
		
			set_initial_b(bb,i,s,c);

			for(ii = 0; ii < V; ii++){

				ttmp = multiply_by_A(bb,ii);
			
				for(ss = 0; ss < 4; ss++){
					for(cc = 0; cc < 3; cc++){		
						matrix[(i*12+s*3+c)*12*V+ii*12+ss*3+cc] = ttmp.s[ss].v[cc];
//						printf("%e %e, ", ttmp.s[ss].v[cc].r, ttmp.s[ss].v[cc].i);
					}
				}
			}

//			printf("\n");

			}
		}
	}

for(i = 0; i < V; i++){
	for(s = 0; s < 4; s++){
		for(c = 0; c < 3; c++){
		
			set_initial_b(bb,i,s,c);

			for(ii = 0; ii < V; ii++){

				bb[ii].multiply_by_gamma5();

			}

			for(ii = 0; ii < V; ii++){
			
				ttmp = multiply_by_A(bb,ii);

				ttmp.multiply_by_gamma5();
			
				for(ss = 0; ss < 4; ss++){
					for(cc = 0; cc < 3; cc++){		
						matrix_dagger[(i*12+s*3+c)*12*V+ii*12+ss*3+cc] = ttmp.s[ss].v[cc];
//						printf("%e %e, ", ttmp.s[ss].v[cc].r, ttmp.s[ss].v[cc].i);
					}
				}
			}

//			printf("\n");

			}
		}
	}

complex t;

for(i = 0; i < V; i++){
	for(s = 0; s < 4; s++){
		for(c = 0; c < 3; c++){
		
			for(ii = 0; ii < V; ii++){
				for(ss = 0; ss < 4; ss++){
					for(cc = 0; cc < 3; cc++){		
//						t = matrix[(i*12+s*3+c)*12*V+ii*12+ss*3+cc] -
//							conjg(matrix_dagger[(ii*12+ss*3+cc)*12*V+i*12+s*3+c]);
//						printf("%e %e\n", t.r, t.i);
						if( fabs(matrix[(i*12+s*3+c)*12*V+ii*12+ss*3+cc].r) > 10e-12 || fabs(matrix[(i*12+s*3+c)*12*V+ii*12+ss*3+cc].i) > 10e-12 ||
						    fabs(matrix_dagger[(ii*12+ss*3+cc)*12*V+i*12+s*3+c].r) > 10e-12 || fabs(matrix_dagger[(ii*12+ss*3+cc)*12*V+i*12+s*3+c].i) > 10e-12 )
						printf("i = %i, s = %i, c = %i, ii = %i, ss = %i, cc = %i,  %e %e      %e %e\n", i, s, c, ii, ss, cc, matrix[(i*12+s*3+c)*12*V+ii*12+ss*3+cc].r, matrix[(i*12+s*3+c)*12*V+ii*12+ss*3+cc].i, matrix_dagger[(ii*12+ss*3+cc)*12*V+i*12+s*3+c].r, matrix_dagger[(ii*12+ss*3+cc)*12*V+i*12+s*3+c].i);
				}
				}
			}

//			printf("\n");

			}
		}
	}

}
*/
int test_neighbour(void){

int i, i_new;

for(i = 0; i < V; i++){

	int dir;

	for(dir = 0; dir < 4; dir++){

	int t = (int)(i/(N*N*N));
	int z = (int)(((i)%(N*N*N))/(N*N));
	int y = (int)(((i)%(N*N*N))%(N*N)/N);
	int x = (int)((((i)%(N*N*N))%(N*N))%N);

	i_new = neighbour(i,LEFT,dir);

	int t_new = (int)(i_new/(N*N*N));
	int z_new = (int)(((i_new)%(N*N*N))/(N*N));
	int y_new = (int)(((i_new)%(N*N*N))%(N*N)/N);
	int x_new = (int)((((i_new)%(N*N*N))%(N*N))%N);

	printf("i = %i, dir = %i, x = %i, y = %i, z = %i, t = %i, left = %i,  x_new = %i, y_new = %i, z_new = %i, t_new = %i \n", i, dir, x, y, z, t, neighbour(i,LEFT,dir), x_new, y_new, z_new, t_new);

	i_new = neighbour(i,RIGHT,dir);

	t_new = (int)(i_new/(N*N*N));
	z_new = (int)(((i_new)%(N*N*N))/(N*N));
	y_new = (int)(((i_new)%(N*N*N))%(N*N)/N);
	x_new = (int)((((i_new)%(N*N*N))%(N*N))%N);

	printf("i = %i, dir = %i, x = %i, y = %i, z = %i, t = %i, left = %i,  x_new = %i, y_new = %i, z_new = %i, t_new = %i \n", i, dir, x, y, z, t, neighbour(i,RIGHT,dir), x_new, y_new, z_new, t_new);

	}

//	printf("i = %i, x = %i, y = %i, z = %i, t = %i, right = %i, x_new = %i, y_new = %i\n", i, x, y, z, t, neighbour(i,RIGHT,1),  neighbour(i,RIGHT,1)%N, (neighbour(i,RIGHT,1)/N)%N);
//	printf("i = %i, left(up) = %i, right(down) = %i\n", i, neighbour(i,LEFT,1), neighbour(i,RIGHT,1));
}
return 1;
}

int main(void){

	int i,j;

	double rsold, rsoldi, rsnew, rsnewi;
	double residuum = 10e-12;
	complex scalarp, scalarq;
	complex tmpsum;
	double tmpsumr;

	set_initial_U(U,'r');
	set_initial_x(x,'u');
	set_initial_b(b,0,0,0);

	tmpsum = 0;

	for(i = 0; i < V; i++){

		scalarp = dagger(x[i])*x[i];

		tmpsum = tmpsum + scalarp;

		tmp_dagger[i] = multiply_by_A(x, i);

	}

	scalarp = 0;
	scalarq = 0;

	for(i = 0; i < V; i++){

		tmp.setZero();
		tmp = multiply_by_Adagger(tmp_dagger, i);

//initial choice
		r[i] = b[i] - tmp;
		p[i] = r[i];

		scalarq = scalarq + dagger(r[i])*r[i];
//		scalarq = scalarq + dagger(b[i])*b[i];

	}

	printf("check <r|r> norm: %e +i %e\n", scalarq.r, scalarq.i);

	rsold = scalarq.r;

	int iter = 0;
	int iter_max = 10000;

	double pAp = 0;
	double pApi = 0;

	double alpha, beta;

	for(iter = 0; iter < iter_max; iter++){

		for(i = 0; i < V; i++){

			tmp_dagger[i] = multiply_by_A(p, i);

		}

		pAp = 0; pApi = 0;

		for(i = 0; i < V; i++){

			scalarp = dagger(p[i])*multiply_by_Adagger(tmp_dagger, i);

			pAp = pAp + scalarp.r;
			pApi = pApi + scalarp.i;
		}

		printf("check <p| A p> norm: %e +i %e\n", pAp, pApi);

		alpha = rsold/pAp;

		for(i = 0; i < V; i++){

			x[i] = x[i] + alpha*p[i];

			r[i] = r[i] - alpha*multiply_by_Adagger(tmp_dagger, i);

		}

		rsnew = 0;
		rsnewi = 0;

		double testa = 0, testb = 0;

		for(i = 0; i < V; i++){

			scalarp = dagger(r[i])*r[i];

			rsnew = rsnew + scalarp.r;
			rsnewi = rsnewi + scalarp.i;

		}


		printf("check <r|r> norm: %e +i %e\n", rsnew, rsnewi);
		
		if( rsnew < residuum )
			break;

		beta = rsnew/rsold;


		for(i = 0; i < V; i++){

			p[i] = r[i] + beta*p[i];

		}


		rsold = rsnew;

		printf("iter = %i, rsnew = %e +i %e\n", iter, rsnew, rsnewi);

	}

	printf("FINAL: iter = %i, rsnew = %e +i %e\n", iter, rsnew, rsnewi);

return 1;
}
